import os
import sys
import traceback
from collections import namedtuple
from enum import IntEnum
from typing import Optional, List

import pymodbus.exceptions
from django.conf import settings
from django.db import models, transaction
from django.template.defaultfilters import floatformat
from django.urls import reverse
from django.utils import timezone
from django.utils.timezone import localtime
from django.utils.translation import gettext_lazy as _
from pyvisa.errors import VisaIOError

from am3000_reader.reader import AM3000Reader
from core import client
from gpm8213_reader.reader import GPM8213Reader
from modbus_reader.reader import Reader
from pro1_reader.reader import Pro1Reader
from pro380_reader.reader import Pro380Reader
from vaconnx_reader.reader import VaconNXReader


class Meter(models.Model):

    VOLTAGE_DECIMALS = 1
    CURRENT_DECIMALS = 3
    POWER_DECIMALS = 3
    ENERGY_DECIMALS = 6

    ip = models.GenericIPAddressField(verbose_name=_("IP Address"), protocol="IPv4", unique=True)
    reader_type = models.CharField(
        verbose_name=_("Reader Type"),
        choices=(
            ("am3000", "AMx000"),  # works for both AM3000 and AM1000
            ("pro380", "Pro380"),
            ("pro1", "Pro1"),
            ("vacon_nx", "Vacon NX"),
            ("gpm8213", "GPM 8213"),
        ),
        max_length=10,
    )
    save_continuously = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("Meter")
        verbose_name_plural = _("Meters")

    def __str__(self):
        return _("Meter at address {}").format(self.ip)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._reader: Optional[Reader] = None
        self.checked = False

    def get_reader_class(self):
        if self.reader_type == "pro380":
            return Pro380Reader
        elif self.reader_type == "pro1":
            return Pro1Reader
        elif self.reader_type == "am3000":
            return AM3000Reader
        elif self.reader_type == "vacon_nx":
            return VaconNXReader
        elif self.reader_type == "gpm8213":
            return GPM8213Reader
        else:
            raise ValueError(_("Unknown reader type"))

    def get_reader_kwargs(self):
        if self.reader_type == "gpm8213":
            return {"check_format": not GPM8213FormatChecked.objects.filter(meter=self).exists()}
        return {}

    @property
    def reader(self) -> Reader:
        if self._reader is not None:
            return self._reader

        self._reader = self.get_reader_class()(self.ip, **self.get_reader_kwargs())

        return self._reader

    def can_connect(self):
        if os.environ.get("MOCK_READER", "false") == "true":
            return True

        try:
            return self.reader.can_connect()
        except pymodbus.exceptions.ConnectionException:
            return False
        except (OSError, VisaIOError):
            return False

    def read(self, *, permanent=False, raise_exceptions=False):
        try:
            response = self.reader.read()
        except pymodbus.exceptions.ConnectionException:
            if raise_exceptions:
                raise
            print("{}: Can't connect to meter {}".format(timezone.now(), self.ip), file=sys.stderr, flush=True)
            return
        except VisaIOError:
            if raise_exceptions:
                raise
            print("{}: Can't read from meter {}".format(timezone.now(), self.ip), file=sys.stderr, flush=True)
            return
        except Exception as e:
            client.captureException()
            print("{}: {}".format(timezone.now(), e), file=sys.stderr, flush=True)
            if settings.DEBUG:
                print(traceback.format_exc())
            if raise_exceptions:
                raise
            return

        if self.reader_type == "gpm8213" and not self.checked:
            self.checked = True
            GPM8213FormatChecked.objects.create(meter=self, created=timezone.now())

        now = timezone.now()

        record = self.record_class(meter=self, created=now)

        for attribute, value in response.items():
            setattr(record, attribute.name, value)

        with transaction.atomic():
            if permanent:
                record.permanent = True
            else:
                save_continuously = Meter.objects.values("save_continuously").get(pk=self.pk)["save_continuously"]

                if (
                    save_continuously
                    and not self.records.filter(created__gte=now.replace(second=0, microsecond=0)).exists()
                ):
                    self.records.filter(permanent=False).delete()

                    record.permanent = True

            record.save()

        return record

    def get_absolute_url(self):
        return reverse("meter:detail", args=[self.ip])

    def show_serial_number(self):
        return self.reader_type == "pro380"

    def one_phase(self):
        return self.reader_type == "pro1"

    @property
    def records(self):
        if self.reader_type == "vacon_nx":
            return self.vaconnxrecord_set
        if self.reader_type == "gpm8213":
            return self.gpm8213record_set
        return self.record_set

    @property
    def record_class(self):
        if self.reader_type == "vacon_nx":
            return VaconNXRecord
        if self.reader_type == "gpm8213":
            return GPM8213Record
        return Record

    Register = namedtuple("Register", "register identifier value")

    def get_all_register_values(self) -> List[Register]:
        reader = self.reader

        registers = []

        for definition in reader.attributes_enum:  # type: IntEnum
            registers.append(self.Register(definition.value, definition.name, reader.read_attribute(definition)))

        return registers


class BaseRecord(models.Model):
    meter = models.ForeignKey(Meter, on_delete=models.SET_NULL, null=True)
    created = models.DateTimeField(db_index=True)
    permanent = models.BooleanField(default=False, db_index=True)

    class Meta:
        abstract = True

    def serialize(self):
        raise NotImplementedError


class Record(BaseRecord):

    serial_number = models.CharField(max_length=8)

    l1_voltage = models.FloatField()
    l2_voltage = models.FloatField(null=True)
    l3_voltage = models.FloatField(null=True)
    l1_current = models.FloatField()
    l2_current = models.FloatField(null=True)
    l3_current = models.FloatField(null=True)
    l1_active_power = models.FloatField()
    l2_active_power = models.FloatField(null=True)
    l3_active_power = models.FloatField(null=True)
    total_active_power = models.FloatField()
    total_active_energy = models.FloatField()

    # FOR PRO1
    @property
    def voltage(self):
        return self.l1_voltage

    @voltage.setter
    def voltage(self, x):
        self.l1_voltage = x

    @property
    def current(self):
        return self.l1_current

    @current.setter
    def current(self, x):
        self.l1_current = x

    @property
    def active_power(self):
        return self.l1_active_power

    @active_power.setter
    def active_power(self, x):
        self.l1_active_power = x
        self.total_active_power = x

    @property
    def active_energy(self):
        return self.total_active_energy

    @active_energy.setter
    def active_energy(self, x):
        self.total_active_energy = x

    # END PRO1

    def serialize(self, one_phase=False):
        x = {
            "date": localtime(self.created).strftime("%d. %m. %Y"),
            "time": localtime(self.created).strftime("%H:%M:%S"),
            "serial-number": self.serial_number,
            "l1-voltage": floatformat(self.l1_voltage, Meter.VOLTAGE_DECIMALS),
            "l1-current": floatformat(self.l1_current, Meter.CURRENT_DECIMALS),
            "l1-power": floatformat(self.l1_active_power, Meter.POWER_DECIMALS),
            "total-energy": floatformat(self.total_active_energy, Meter.ENERGY_DECIMALS),
        }

        if not one_phase:
            x.update(
                {
                    "l2-voltage": floatformat(self.l2_voltage, Meter.VOLTAGE_DECIMALS),
                    "l3-voltage": floatformat(self.l3_voltage, Meter.VOLTAGE_DECIMALS),
                    "l2-current": floatformat(self.l2_current, Meter.CURRENT_DECIMALS),
                    "l3-current": floatformat(self.l3_current, Meter.CURRENT_DECIMALS),
                    "l2-power": floatformat(self.l2_active_power, Meter.POWER_DECIMALS),
                    "l3-power": floatformat(self.l3_active_power, Meter.POWER_DECIMALS),
                    "total-power": floatformat(self.total_active_power, Meter.POWER_DECIMALS),
                }
            )

        return x


class VaconNXRecord(BaseRecord):
    motor_torque = models.FloatField()
    motor_power = models.FloatField()
    motor_speed = models.IntegerField()
    freq_out = models.FloatField()
    motor_current = models.FloatField()
    motor_voltage = models.FloatField()
    dc_voltage = models.IntegerField()

    @property
    def voltage_ac(self):
        return floatformat(self.motor_voltage, 1)

    @property
    def current(self):
        return floatformat(self.motor_current, 1)

    @property
    def voltage_dc(self):
        return str(self.dc_voltage)

    @property
    def frequency(self):
        return floatformat(self.freq_out, 2)

    @property
    def speed(self):
        return str(self.motor_speed)

    @property
    def torque(self):
        return floatformat(self.motor_torque, 1)

    @property
    def power(self):
        return floatformat(self.motor_power, 1)

    def serialize(self):
        x = {
            "date": localtime(self.created).strftime("%d. %m. %Y"),
            "time": localtime(self.created).strftime("%H:%M:%S"),
            "voltage-ac": self.voltage_ac,
            "current": self.current,
            "voltage-dc": self.voltage_dc,
            "frequency": self.frequency,
            "speed": self.speed,
            "torque": self.torque,
            "power": self.power,
            "ip-address": self.meter.ip,
        }

        return x


class GPM8213Record(BaseRecord):
    voltage = models.FloatField(null=True)
    current = models.FloatField(null=True)
    active_power = models.FloatField(null=True)
    watt_hours = models.FloatField(null=True)
    ampere_hours = models.FloatField(null=True)
    measurement_time = models.FloatField(null=True)

    def serialize(self):
        x = {
            "date": localtime(self.created).strftime("%d. %m. %Y"),
            "time": localtime(self.created).strftime("%H:%M:%S"),
            "voltage": "" if self.voltage is None else floatformat(self.voltage, 3),
            "current": "" if self.current is None else floatformat(self.current, 5),
            "active_power": "" if self.active_power is None else floatformat(self.active_power, 4),
            "watt_hours": "" if self.watt_hours is None else floatformat(self.watt_hours, 4),
            "ampere_hours": "" if self.ampere_hours is None else floatformat(self.ampere_hours, 4),
            "measurement_time": "" if self.measurement_time is None else int(self.measurement_time),
            "ip-address": self.meter.ip,
        }

        return x


class GPM8213FormatChecked(models.Model):
    meter = models.ForeignKey(Meter, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_created=True)

    class Meta:
        verbose_name = "GPM 8213 Format Checked"
        verbose_name_plural = "GPM 8213s Format Checked"
