from django import template

from meter.models import Record

register = template.Library()


@register.filter
def voltage(obj: Record, phase: str):
    return getattr(obj, "{}_voltage".format(phase), None)


@register.filter
def current(obj: Record, phase: str):
    return getattr(obj, "{}_current".format(phase), None)


@register.filter
def active_power(obj: Record, phase: str):
    return getattr(obj, "{}_active_power".format(phase), None)
