from django.urls import path

from meter.views import (
    MeterView,
    ClearMeterView,
    ExportRecordsView,
    DataView,
    SaveCurrentView,
    ChangeSaveContinuouslyView,
    AllRegistersView,
)

app_name = "meter"

urlpatterns = [
    path("<ipv4:ip>/", MeterView.as_view(), name="detail"),
    path("<ipv4:ip>/registers/", AllRegistersView.as_view(), name="registers"),
    path("<ipv4:ip>/clear/", ClearMeterView.as_view(), name="clear"),
    path("<ipv4:ip>/export/", ExportRecordsView.as_view(), name="export"),
    path("<ipv4:ip>/data/", DataView.as_view(), name="data"),
    path("<ipv4:ip>/save_current/", SaveCurrentView.as_view(), name="save_current"),
    path("<ipv4:ip>/change_save_continuously/", ChangeSaveContinuouslyView.as_view(), name="change_save_continuously"),
]
