import csv

import pymodbus.exceptions
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, JsonResponse, HttpResponse
from django.utils.timezone import localtime
from django.utils.translation import gettext
from django.views import View
from django.views.generic import TemplateView
from django.db import transaction

from meter.models import Meter, BaseRecord, GPM8213Record


class MeterTemplateView(TemplateView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.meter = None
        self.can_connect = False

    def get(self, request, *args, **kwargs):
        try:
            self.meter = Meter.objects.get(ip=str(kwargs["ip"]))
        except ObjectDoesNotExist:
            raise Http404

        self.can_connect = self.meter.can_connect()

        return super().get(request, *args, **kwargs)


class MeterView(MeterTemplateView):

    template_name = "meter.html"

    def get_template_names(self):
        if self.can_connect:
            if self.meter.reader_type == "vacon_nx":
                return "vacon_nx_meter.html"
            if self.meter.reader_type == "gpm8213":
                return "gpm8213_meter.html"
            return self.template_name
        return "meter_unavailable.html"

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        data["meter"] = self.meter
        data["can_connect"] = self.can_connect

        try:
            data["last_record"] = self.meter.records.latest("created")
        except ObjectDoesNotExist:
            data["last_record"] = None

        if self.meter.reader_type not in ["vacon_nx", "gpm8213"]:

            if self.meter.one_phase():
                data["phases"] = ("l1",)
            else:
                data["phases"] = "l1", "l2", "l3"

            data["one_phase"] = self.meter.one_phase()

            data["voltage_decimals"] = Meter.VOLTAGE_DECIMALS
            data["current_decimals"] = Meter.CURRENT_DECIMALS
            data["power_decimals"] = Meter.POWER_DECIMALS
            data["energy_decimals"] = Meter.ENERGY_DECIMALS

        return data


class AllRegistersView(MeterTemplateView):
    def get_template_names(self):
        if self.can_connect:
            return "all_registers.html"
        return "meter_unavailable.html"

    def get_context_data(self, **kwargs):
        data = super().get_context_data()

        if self.can_connect:
            data["registers"] = self.meter.get_all_register_values()

        return data


class ClearMeterView(View):
    def post(self, request, ip):
        try:
            meter = Meter.objects.get(ip=str(ip))
        except ObjectDoesNotExist:
            raise Http404

        meter.records.all().delete()

        return JsonResponse({"success": True})


class DataView(View):
    def get(self, request, ip):
        try:
            meter = Meter.objects.get(ip=str(ip))
        except ObjectDoesNotExist:
            raise Http404

        try:
            record: BaseRecord = meter.records.latest("created")
        except ObjectDoesNotExist:
            return JsonResponse({"record": None, "save_continuously": meter.save_continuously})

        return JsonResponse({"record": record.serialize(), "save_continuously": meter.save_continuously})


class SaveCurrentView(View):
    def post(self, request, ip):
        try:
            meter = Meter.objects.get(ip=str(ip))
        except ObjectDoesNotExist:
            raise Http404

        if meter.save_continuously:
            return JsonResponse({"error": gettext("Values are being saved continuously right now.")}, status=400)

        if meter.reader_type == "gpm8213":
            try:
                with transaction.atomic():
                    record = meter.records.select_for_update().latest("created")
                    record.permanent = True
                    record.save()

                    meter.records.filter(permanent=False).delete()
            except ObjectDoesNotExist:
                return JsonResponse({"error": gettext("Can't connect to the meter.")}, status=400)
        else:
            try:
                record: BaseRecord = meter.read(permanent=True, raise_exceptions=True)
            except pymodbus.exceptions.ConnectionException:
                return JsonResponse({"error": gettext("Can't connect to the meter.")}, status=400)

            meter.records.filter(permanent=False).delete()

        return JsonResponse({"record": record.serialize()})


class ChangeSaveContinuouslyView(View):
    def post(self, request, ip):
        try:
            meter = Meter.objects.get(ip=str(ip))
        except ObjectDoesNotExist:
            raise Http404

        meter.save_continuously = not meter.save_continuously
        meter.save()

        return JsonResponse({"save_continuously": meter.save_continuously})


class ExportRecordsView(View):
    def get(self, request, ip):
        try:
            meter = Meter.objects.get(ip=str(ip))
        except ObjectDoesNotExist:
            raise Http404

        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="{}.csv"'.format(meter.ip)

        writer = csv.writer(response)

        if meter.reader_type == "vacon_nx":
            self.export_vacon_nx(meter, writer)
        elif meter.reader_type == "gpm8213":
            self.export_gpm8213(meter, writer)
        else:
            self.export(meter, writer)

        return response

    def export(self, meter: Meter, writer):
        one_phase = meter.one_phase()

        header_row = [
            gettext("Time"),
            gettext("L1 Voltage") if not one_phase else gettext("Voltage"),
            gettext("L2 Voltage") if not one_phase else None,
            gettext("L3 Voltage") if not one_phase else None,
            gettext("L1 Current") if not one_phase else gettext("Current"),
            gettext("L2 Current") if not one_phase else None,
            gettext("L3 Current") if not one_phase else None,
            gettext("L1 Active Power") if not one_phase else gettext("Active Power"),
            gettext("L2 Active Power") if not one_phase else None,
            gettext("L3 Active Power") if not one_phase else None,
            gettext("Total Active power") if not one_phase else None,
            gettext("Total Active energy"),
        ]

        header_row = [x for x in header_row if x is not None]

        if meter.show_serial_number():
            header_row.insert(1, gettext("Serial Number"))

        writer.writerow(header_row)

        for record in meter.records.filter(permanent=True).order_by("created"):
            row = [
                localtime(record.created).isoformat(),
                round(record.l1_voltage, Meter.VOLTAGE_DECIMALS),
                round(record.l2_voltage, Meter.VOLTAGE_DECIMALS) if not one_phase else None,
                round(record.l3_voltage, Meter.VOLTAGE_DECIMALS) if not one_phase else None,
                round(record.l1_current, Meter.CURRENT_DECIMALS),
                round(record.l2_current, Meter.CURRENT_DECIMALS) if not one_phase else None,
                round(record.l3_current, Meter.CURRENT_DECIMALS) if not one_phase else None,
                round(record.l1_active_power, Meter.POWER_DECIMALS),
                round(record.l2_active_power, Meter.POWER_DECIMALS) if not one_phase else None,
                round(record.l3_active_power, Meter.POWER_DECIMALS) if not one_phase else None,
                round(record.total_active_power, Meter.POWER_DECIMALS) if not one_phase else None,
                round(record.total_active_energy, Meter.ENERGY_DECIMALS),
            ]

            row = [x for x in row if x is not None]

            if meter.show_serial_number():
                row.insert(1, record.serial_number)

            writer.writerow(row)

    def export_vacon_nx(self, meter: Meter, writer):
        header_row = [
            gettext("Time"),
            gettext("Voltage AC [V]"),
            gettext("Current [A]"),
            gettext("Voltage DC [V]"),
            gettext("Frequency [Hz]"),
            gettext("Speed [rpm]"),
            gettext("Torque [%]"),
            gettext("Power [%]"),
        ]

        writer.writerow(header_row)

        for record in meter.records.filter(permanent=True).order_by("created"):
            row = [
                localtime(record.created).isoformat(),
                record.voltage_ac,
                record.current,
                record.voltage_dc,
                record.frequency,
                record.speed,
                record.torque,
                record.power,
            ]

            row = [x for x in row if x is not None]

            if meter.show_serial_number():
                row.insert(1, record.serial_number)

            writer.writerow(row)

    def export_gpm8213(self, meter, writer):
        header_row = [
            gettext("Timestamp"),
            gettext("Voltage [V]"),
            gettext("Current [A]"),
            gettext("Active Power [W]"),
            gettext("WattHours [Wh]"),
            gettext("AmpereHours [Ah]"),
            gettext("Time [s]"),
        ]

        writer.writerow(header_row)

        for record in meter.records.filter(permanent=True).order_by("created"):  # type: GPM8213Record
            row = [
                localtime(record.created).isoformat(),
                record.voltage,
                record.current,
                record.active_power,
                record.watt_hours,
                record.ampere_hours,
                record.measurement_time,
            ]

            row = [x for x in row if x is not None]

            writer.writerow(row)
