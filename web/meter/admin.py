from django.contrib import admin, messages
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from meter.models import Meter, GPM8213FormatChecked


def go_to_registers(modeladmin, request, queryset):
    if queryset.count() != 1:
        messages.error(request, _("Select one device"))
        return

    return redirect(reverse("meter:registers", args=[queryset.get().ip]))


class MeterAdmin(admin.ModelAdmin):

    list_display = ("reader_type", "ip")
    fields = ("ip", "reader_type")

    actions = [go_to_registers]


class GPM8213FormatCheckedAdmin(admin.ModelAdmin):
    list_display = ("meter", "created")


admin.site.register(Meter, MeterAdmin)
admin.site.register(GPM8213FormatChecked, GPM8213FormatCheckedAdmin)
