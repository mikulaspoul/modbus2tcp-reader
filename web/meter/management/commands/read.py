import threading
import time

import schedule
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand
from django.utils import timezone

from meter.models import Meter


class Command(BaseCommand):
    help = "Runs the periodic reading from the meters"

    def handle(self, *args, **options):
        print("{}: Booted up read command".format(timezone.now()), flush=True)

        meters = []

        def run_threaded(job_func):
            job_thread = threading.Thread(target=job_func)
            job_thread.start()

        def reschedule():
            print("{}: Rescheduling readers".format(timezone.now()), flush=True)

            new_meters = []

            for meter in meters:
                try:
                    Meter.objects.get(id=meter.id)
                    new_meters.append(meter)
                except ObjectDoesNotExist:
                    schedule.clear(f"meter-{meter.id}")

            meters.clear()
            meters.extend(new_meters)

            for meter in Meter.objects.exclude(pk__in=[x.id for x in meters]):
                meters.append(meter)
                schedule.every(5).seconds.do(run_threaded, meter.read).tag(f"meter-{meter.id}")

        reschedule()

        schedule.every().minute.do(reschedule)

        while True:
            schedule.run_pending()
            time.sleep(0.01)
