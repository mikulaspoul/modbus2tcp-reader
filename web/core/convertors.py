from ipaddress import IPv4Address


class IPv4Converter:
    regex = r"(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])"

    def to_python(self, value: str) -> IPv4Address:
        return IPv4Address(value)

    def to_url(self, value: IPv4Address) -> str:
        return str(value)
