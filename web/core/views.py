from django.views.generic import TemplateView

from meter.models import Meter


class IndexView(TemplateView):

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        data["meters"] = Meter.objects.all()

        return data
