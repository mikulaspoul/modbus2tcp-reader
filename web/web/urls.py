from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, register_converter, include

from core.convertors import IPv4Converter
from core.views import IndexView

admin.autodiscover()

register_converter(IPv4Converter, "ipv4")

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("meter/", include("meter.urls", namespace="meter")),
    path("admin/", admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
