import os
from raven import Client
from raven.middleware import Sentry


from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "web.settings")


try:
    from .local_settings import RAVEN_DSN
except ImportError:
    RAVEN_DSN = None

application = get_wsgi_application()

if RAVEN_DSN:
    application = Sentry(get_wsgi_application(), Client(RAVEN_DSN))
