$(document).ready ->
  values = $(".record-value")

  save_continuously = window.save_continuously
  freeze_updates = false

  update_now_button = $("#update-now")
  save_values_button = $("#save-values")
  export_button = $("#export")
  clear_data_button = $("#clear-data")

  clear_data_button.on("click", ->
    $.ajax({
      url: window.urls.clear_data,
      method: "POST"
    }).done(->
      values.text("")
    ).fail(->
      alert(window.texts.clear_failed)
    )
  )

  export_button.on("click", ->
    window.location = window.urls.export_data;
  )

  reload_values = (callback) ->
    $.ajax({
      url: window.urls.data,
      method: "GET",
    }).done( (data) ->
      if not freeze_updates and data.record
        for key, value of data.record
          $("." + key).text(value)

      if data.save_continuously and not save_continuously
        save_continuously = data.save_continuously
        unfreeze()
        update_save_values_button_text()

      callback?()
    ).fail(->
      alert(window.texts.load_failed)
      callback?()
    )

  schedule_reload_values = ->
    setTimeout(->
      reload_values(schedule_reload_values)
    , 2500)

  schedule_reload_values()

  update_now_button_text = ->
    if freeze_updates
      update_now_button.find(".freeze").hide()
      update_now_button.find(".unfreeze").show()
      update_now_button.removeClass("button-outline")

      save_values_button.prop('disabled', true);
    else
      update_now_button.find(".freeze").show()
      update_now_button.find(".unfreeze").hide()
      update_now_button.addClass("button-outline")

      save_values_button.prop('disabled', false);

  update_save_values_button_text = ->
    if save_continuously
      save_values_button.find(".activate").hide()
      save_values_button.find(".deactivate").show()
      save_values_button.removeClass("button-outline")

      update_now_button.prop('disabled', true);
    else
      save_values_button.find(".activate").show()
      save_values_button.find(".deactivate").hide()
      save_values_button.addClass("button-outline")

      update_now_button.prop('disabled', false);

  if save_continuously
    update_save_values_button_text()

  unfreeze = ->
    freeze_updates = false
    update_now_button_text()
    reload_values()

  update_now_button.on("click", ->
    if freeze_updates
      unfreeze()
      return

    freeze_updates = true
    update_now_button_text()

    $.ajax({
      url: window.urls.save_current,
      method: "POST"
    }).done( (data) ->
      for key, value of data.record
        $("." + key).text(value)

    ).fail( (response) ->
      freeze_updates = false
      update_now_button_text()

      if response.responseJSON
        alert(response.responseJSON.error)
      else
        alert(window.texts.save_current_failed)
    )
  )

  save_values_button.on("click", ->
    $.ajax({
      url: window.urls.change_save_continuously,
      method: "POST",
    }).done((data) ->
      save_continuously = data.save_continuously
      update_save_values_button_text()
    ).fail( (response) ->
      if response.responseJSON
        alert(response.responseJSON.error)
      else
        alert(window.texts.save_continuously_failed)
    )
  )