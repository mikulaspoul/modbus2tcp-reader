from collections import defaultdict
from enum import IntEnum

from modbus_reader.types import DataTypes, Definition


class Pro380Attributes(IntEnum):
    serial_number = 0x1000
    meter_code = 0x1010
    meter_id = 0x1018
    baudrate = 0x1020
    protocol_version = 0x1050
    software_version = 0x1054
    hardware_version = 0x1048
    meter_amps = 0x1060

    ct_rate = 0x1062
    s0_output_rate = 0x1066
    combined_code = 0x107A
    lcd_cycle_time = 0x1510
    parity_setting = 0x1520

    l1_voltage = 0x2008
    l2_voltage = 0x200C
    l3_voltage = 0x2010

    grid_frequency = 0x2020

    l1_current = 0x2068
    l2_current = 0x206C
    l3_current = 0x2070

    total_active_power = 0x2080
    l1_active_power = 0x2088
    l2_active_power = 0x208C
    l3_active_power = 0x2090

    total_reactive_power = 0x20A0
    l1_reactive_power = 0x20A8
    l2_reactive_power = 0x20AC
    l3_reactive_power = 0x20B0

    total_apparent_power = 0x20C0
    l1_apparent_power = 0x20C8
    l2_apparent_power = 0x20CC
    l3_apparent_power = 0x20D0

    power_factor_1 = 0x20E0
    power_factor_2 = 0x20E8
    power_factor_3 = 0x20EC
    power_factor_4 = 0x20D0

    tariff = 0x2200

    total_active_energy = 0x3000
    t1_total_active_energy = 0x3100
    t2_total_active_energy = 0x3200
    l1_total_active_energy = 0x3008
    l2_total_active_energy = 0x300C
    l3_total_active_energy = 0x3010

    forward_active_energy = 0x3020
    t1_forward_active_energy = 0x3120
    t2_forward_active_energy = 0x3220
    l1_forward_active_energy = 0x3028
    l2_forward_active_energy = 0x302C
    l3_forward_active_energy = 0x3030

    reverse_active_energy = 0x3040
    t1_reverse_active_energy = 0x3140
    t2_reverse_active_energy = 0x3240
    l1_reverse_active_energy = 0x3048
    l2_reverse_active_energy = 0x304C
    l3_reverse_active_energy = 0x3050

    total_reactive_energy = 0x3060
    t1_total_reactive_energy = 0x3160
    t2_total_reactive_energy = 0x3260
    l1_reactive_energy = 0x3068
    l2_reactive_energy = 0x306C
    l3_reactive_energy = 0x3080

    forward_reactive_energy = 0x3080
    t1_forward_reactive_energy = 0x3180
    t2_forward_reactive_energy = 0x3280
    l1_forward_reactive_energy = 0x3088
    l2_forward_reactive_energy = 0x308C
    l3_forward_reactive_energy = 0x3090

    reverse_reactive_energy = 0x30A0
    t1_reverse_reactive_energy = 0x31A0
    t2_reverse_reactive_energy = 0x32A0
    l1_reverse_reactive_energy = 0x30A8
    l2_reverse_reactive_energy = 0x30AC
    l3_reverse_reactive_energy = 0x30B0


class Pro380Definitions:
    def __init__(self):
        # most attributes are floats, remainder are listed explicitly
        self.definition = defaultdict(
            lambda: (DataTypes.FLOAT, 2),
            {
                Pro380Attributes.serial_number: (DataTypes.HEX, 2),
                Pro380Attributes.meter_code: (DataTypes.HEX, 1),
                Pro380Attributes.meter_id: (DataTypes.INT, 1),
                Pro380Attributes.baudrate: (DataTypes.INT, 1),
                Pro380Attributes.meter_amps: (DataTypes.INT, 1),
                Pro380Attributes.ct_rate: (DataTypes.HEX, 1),
                Pro380Attributes.combined_code: (DataTypes.INT, 1),
                Pro380Attributes.lcd_cycle_time: (DataTypes.HEX, 1),
                Pro380Attributes.parity_setting: (DataTypes.HEX, 1),
                Pro380Attributes.tariff: (DataTypes.HEX, 1),
            },
        )

    def __call__(self, attribute: Pro380Attributes) -> Definition:
        return Definition(*self.definition[attribute])
