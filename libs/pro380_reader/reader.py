import random
from ipaddress import IPv4Address
from pprint import pprint
from typing import Union, Dict

from modbus_reader.reader import Reader
from pro380_reader.attributes import Pro380Definitions, Pro380Attributes
from modbus_reader.utils import timeit


class Pro380Reader(Reader):

    attributes = [
        Pro380Attributes.l1_voltage,
        Pro380Attributes.l2_voltage,
        Pro380Attributes.l3_voltage,
        Pro380Attributes.l1_current,
        Pro380Attributes.l2_current,
        Pro380Attributes.l3_current,
        Pro380Attributes.l1_active_power,
        Pro380Attributes.l2_active_power,
        Pro380Attributes.l3_active_power,
        Pro380Attributes.total_active_power,
        Pro380Attributes.total_active_energy,
    ]
    attributes_enum = Pro380Attributes
    definition_class = Pro380Definitions

    def __init__(self, ip: IPv4Address, *, unit: int = 1):
        super(Pro380Reader, self).__init__(ip, unit=unit)

        self.serial_number = None

    @timeit
    def read(self) -> Dict[Pro380Attributes, Union[int, float]]:
        if self.serial_number is None:
            self.serial_number = self.read_attribute(Pro380Attributes.serial_number)

        response = {Pro380Attributes.serial_number: self.serial_number}

        response.update(super().read())

        return response

    def generate_mock_value(self, attribute):
        return {
            Pro380Attributes.l1_voltage: 230 * random.uniform(0.999, 1.001),
            Pro380Attributes.l2_voltage: 230 * random.uniform(0.999, 1.001),
            Pro380Attributes.l3_voltage: 230 * random.uniform(0.999, 1.001),
            Pro380Attributes.l1_current: 15 * random.uniform(0.9, 1.1),
            Pro380Attributes.l2_current: 15 * random.uniform(0.9, 1.1),
            Pro380Attributes.l3_current: 15 * random.uniform(0.9, 1.1),
            Pro380Attributes.l1_active_power: 10 * random.uniform(0.8, 1.2),
            Pro380Attributes.l2_active_power: 10 * random.uniform(0.8, 1.2),
            Pro380Attributes.l3_active_power: 10 * random.uniform(0.8, 1.2),
            Pro380Attributes.total_active_power: 30 * random.uniform(0.8, 1.2),
            Pro380Attributes.total_active_energy: 30 * random.uniform(0.8, 1.2),
            Pro380Attributes.serial_number: 10920932,
        }[attribute]


if __name__ == "__main__":

    reader = Pro380Reader(IPv4Address("10.0.0.151"))

    pprint(reader.read())
