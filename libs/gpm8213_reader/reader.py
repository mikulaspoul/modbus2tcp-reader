import logging
import math
import os
import random
import sys
from pprint import pprint
from typing import Dict, Any, Union

import pyvisa
from pyvisa.resources.tcpip import TCPIPSocket
from ipaddress import IPv4Address

from gpm8213_reader.attributes import GPM8213Attributes

pyvisa.log_to_screen()

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(stream=sys.stdout))


class GPM8213Reader:
    header = (
        "Urms,Irms,P,S,Q,PF,deg,fU,fI,U+pK,U-pK,I+pK,I-pK,Time,WP,WP+,WP-,"
        "q,q+,q-,P+pK,P-pK,CFU,CFI,Uthd,Ithd,URange,IRange"
    )
    attributes = [
        GPM8213Attributes.voltage,
        GPM8213Attributes.current,
        GPM8213Attributes.active_power,
        GPM8213Attributes.watt_hours,
        GPM8213Attributes.ampere_hours,
        GPM8213Attributes.measurement_time,
    ]

    def query(self, query):
        logger.info("Writing: %r", query)
        self.instance.write(query)
        response = self.instance.read_bytes(self.instance.chunk_size, break_on_termchar=True).decode("utf-8").rstrip()

        logger.info("Got back: %r", response)

        return response

    def __init__(self, ip: IPv4Address, check_format: True):
        self.ip = ip
        self.check_format = check_format

        self.resource_manager = pyvisa.ResourceManager("@py")
        self._instance: TCPIPSocket = None

    @property
    def instance(self):
        if self._instance is not None:
            try:
                self._instance.session is None
            except pyvisa.errors.InvalidSession:
                self._instance.open()
            return self._instance

        self._instance: TCPIPSocket = self.resource_manager.open_resource(f"TCPIP0::{self.ip}::23::SOCKET")
        self._instance.read_termination = "\r\n"

        if self.check_format:
            # ensure full data is returned
            if self.query(":NUMERIC:NORMAL:HEADER?") != self.header:
                self._instance.write(":NUMERIC:NORMAL:PRESET 4")
            self.check_format = True

        return self._instance

    def read(self) -> Dict[Any, Union[int, float]]:
        if os.environ.get("MOCK_READER", "false") == "true":
            return {
                GPM8213Attributes.voltage: 230 * random.uniform(0.999, 1.001),
                GPM8213Attributes.current: 15 * random.uniform(0.9, 1.1),
                GPM8213Attributes.active_power: 10 * random.uniform(0.8, 1.2),
                GPM8213Attributes.watt_hours: 3 * random.uniform(0.8, 1.2) if random.uniform(0, 1) > 0.5 else None,
                GPM8213Attributes.ampere_hours: 30 * random.uniform(0.8, 1.2) if random.uniform(0, 1) > 0.5 else None,
                GPM8213Attributes.measurement_time: float(int(12 * random.uniform(0.1, 10))),
            }

        try:
            values = self.query(":NUMERIC:NORMAL:VALUE?")
        except (BrokenPipeError, ConnectionResetError):
            self._instance.close()
            raise

        values = {key: value for key, value in zip(self.header.split(","), values.split(","))}

        response = {}

        for attribute in self.attributes:
            value = float(values[attribute.value])
            response[attribute] = None if math.isnan(value) else value

        return response

    def can_connect(self):
        instance = self.resource_manager.open_resource(f"TCPIP0::{self.ip}::23::SOCKET")
        instance.close()

        return True


if __name__ == "__main__":
    reader = GPM8213Reader(IPv4Address("10.0.0.39"))

    pprint(reader.read())
