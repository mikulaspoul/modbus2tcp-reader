from enum import Enum


class GPM8213Attributes(Enum):

    voltage = "Urms"  # U Voltage U
    current = "Irms"  # I Current I
    active_power = "P"  # P Active power P
    apparent_power = "S"  # S Apparent power S
    reactive_power = "Q"  # Q Reactive power Q
    power_factor = "PF"  # LAMBda Power factor λ
    phase_difference = "deg"  # PHI Phase difference Φ
    voltage_frequency = "fU"  # FU Voltage frequency fu
    current_frequency = "fI"  # FI Current frequency fI
    up_peak = "U+pK"  # UPPeak Maximum voltage: U+pk
    um_peak = "U-pK"  # UMPeak Minimum voltage: U-pk
    ip_peak = "I+pK"  # IPPeak Maximum current: I+pk
    im_peak = "I-pK"  # IMPeak Minimum current: I-pk
    measurement_time = "Time"  # TIME Integration time
    watt_hours = "WP"  # WH Watt hour WP
    whp_watt_hours = "WP+"  # WHP Positive watt hour WP+
    whm_watt_hours = "WP-"  # WHM Positive watt hour WP-
    ampere_hours = "q"  # AH Ampere hour q
    ahp_ampere_hours = "q+"  # AHP Positive ampere hour q+
    ahm_ampere_hours = "q-"  # AHM Positive ampere hour q
    pp_peak = "P+pK"  # PPPeak Maximum power: P+pk
    pm_peak = "P-pK"  # PMPeak Minimum power: P-pk
    voltage_factor = "CFU"  # CFU Voltage factor λ
    current_factor = "CFI"  # CFV Current factor λ
    voltage_distortion = "Uthd"  # UTHD Total harmonic distortion of voltage Uthd
    current_distortion = "Ithd"  # ITHD Total harmonic distortion of current
    voltage_range = "URange"  # URANge Voltage range
    current_range = "IRange"  # IRANge Current range
