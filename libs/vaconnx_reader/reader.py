import random
from ipaddress import IPv4Address
from pprint import pprint
from typing import Dict, Any, Union

from modbus_reader.reader import Reader
from vaconnx_reader.attributes import VaconNXAttributes, VaconNXDefinitions


class VaconNXReader(Reader):

    attributes = [
        VaconNXAttributes.motor_torque,
        VaconNXAttributes.motor_power,
        VaconNXAttributes.motor_speed,
        VaconNXAttributes.freq_out,
        VaconNXAttributes.motor_current,
        VaconNXAttributes.motor_voltage,
        VaconNXAttributes.dc_voltage,
    ]
    attributes_enum = VaconNXAttributes
    definition_class = VaconNXDefinitions

    def __init__(self, ip: IPv4Address):
        super().__init__(ip, unit=0)

    def read(self) -> Dict[Any, Union[int, float]]:
        response = super().read()

        response[VaconNXAttributes.freq_out] /= 100
        response[VaconNXAttributes.motor_torque] /= 10
        response[VaconNXAttributes.motor_power] /= 10
        response[VaconNXAttributes.motor_current] /= 10
        response[VaconNXAttributes.motor_voltage] /= 10

        return response

    def generate_mock_value(self, attribute):
        return int(
            {
                VaconNXAttributes.motor_torque: 1000 * random.uniform(0.8, 1.2),
                VaconNXAttributes.motor_power: 1200 * random.uniform(0.8, 1.2),
                VaconNXAttributes.motor_speed: 1500 * random.uniform(0.8, 1.2),
                VaconNXAttributes.freq_out: 5000 * random.uniform(0.999, 1.001),
                VaconNXAttributes.motor_current: 100 * random.uniform(0.8, 1.2),
                VaconNXAttributes.motor_voltage: 4000 * random.uniform(0.99, 1.01),
                VaconNXAttributes.dc_voltage: 560 * random.uniform(0.99, 1.01),
            }[attribute]
        )


if __name__ == "__main__":
    reader = VaconNXReader(IPv4Address("10.0.0.151"))

    pprint(reader.read())
