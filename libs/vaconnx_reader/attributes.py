from collections import defaultdict
from enum import IntEnum

from modbus_reader.types import DataTypes, Definition


class VaconNXAttributes(IntEnum):

    motor_torque = 10301
    motor_power = 10302
    motor_speed = 10303
    freq_out = 10304
    freq_ref = 10305
    remote_indication = 10306
    motor_control_mode = 10307
    active_fault = 10308
    motor_current = 10309
    motor_voltage = 10310
    freq_min = 10311
    freq_scale = 10312
    dc_voltage = 10313
    motor_nom_current = 10314
    motor_nom_voltage = 10315
    motor_nom_freq = 10316
    motor_nom_speed = 10317
    current_scale = 10318
    motor_current_limit = 10319
    deceleration_time = 10320
    acceleration_time = 10321
    freq_max = 10322
    pole_pair_number = 10323
    ramp_time_scale = 10324
    ms_counter = 10325


class VaconNXDefinitions:
    def __init__(self):
        # all of the attributes are integers, some signed/unsigned normal/short, but here it probably doesn't matter
        self.definition = defaultdict(lambda: (DataTypes.INT, 1))

    def __call__(self, attribute: VaconNXAttributes) -> Definition:
        return Definition(*self.definition[attribute])
