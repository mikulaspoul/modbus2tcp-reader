import os
from ipaddress import IPv4Address
from typing import Any, Dict, Union

from pymodbus.client.sync import ModbusTcpClient

from .exceptions import ErrorResponseException


class Reader:

    attributes = []
    attributes_enum = None
    definition_class = None

    def __init__(self, ip: IPv4Address, *, unit: int = 1):
        self.ip = ip
        self.client = ModbusTcpClient(host=str(self.ip), port=502)
        self.definitions = self.definition_class()
        self.unit = unit

    def generate_mock_value(self, attribute):
        raise NotImplementedError

    def read_attribute(self, attribute):
        definition = self.definitions(attribute)

        if os.environ.get("MOCK_READER", "false") == "true":
            return self.generate_mock_value(attribute)

        response = self.client.read_holding_registers(attribute.value, definition.length, unit=self.unit)

        if response.isError():
            if isinstance(response, Exception):
                raise response

            raise ErrorResponseException(response, f"Can't read attribute {attribute} from IP address {self.ip}")

        return definition.type.from_registers(response.registers)

    def read(self) -> Dict[Any, Union[int, float]]:
        response = {}

        for attribute in self.attributes:
            response[attribute] = self.read_attribute(attribute)

        self.client.close()

        return response

    def can_connect(self):
        x = self.client.connect()
        self.client.close()
        return x
