from collections import namedtuple
from typing import List

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder


class DataType:
    def from_registers(self, registers: List[int]):
        raise NotImplementedError()


class HexDataType(DataType):
    def from_registers(self, registers: List[int]) -> str:
        if len(registers) == 1:
            return "{:x}".format(registers[0])
        elif len(registers) == 2:
            return "{:x}{:04x}".format(registers[0], registers[1])

        raise ValueError("Invalid number of registers")


class FloatDataType(DataType):
    def from_registers(self, registers: List[int]) -> float:
        decoder = BinaryPayloadDecoder.fromRegisters(registers, byteorder=Endian.Big)

        if len(registers) == 2:
            return decoder.decode_32bit_float()
        elif len(registers) == 4:
            return decoder.decode_64bit_float()

        raise ValueError("Invalid number of registers")


class IntDataType(DataType):
    def from_registers(self, registers: List[int]) -> int:
        if len(registers) == 1:
            return int(registers[0])

        raise ValueError("Invalid number of registers")


class DataTypes:

    HEX = HexDataType()
    FLOAT = FloatDataType()
    INT = IntDataType()


Definition = namedtuple("Definition", ["type", "length"])
