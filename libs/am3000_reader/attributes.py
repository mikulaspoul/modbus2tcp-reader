from collections import defaultdict
from enum import IntEnum

from modbus_reader.types import Definition, DataTypes


class AM3000Attributes(IntEnum):

    # this list is not full, only the used stuff were put here

    l1_voltage = 102
    l2_voltage = 104
    l3_voltage = 106

    l1_current = 118
    l2_current = 120
    l3_current = 122

    l1_active_power = 128
    l2_active_power = 130
    l3_active_power = 132

    total_active_power = 126
    total_active_energy = 4100


class AM3000Definitions:
    def __init__(self):
        # all of the used attributes are floats
        self.definition = defaultdict(lambda: (DataTypes.FLOAT, 2))

    def __call__(self, attribute: AM3000Attributes) -> Definition:
        return Definition(*self.definition[attribute])
