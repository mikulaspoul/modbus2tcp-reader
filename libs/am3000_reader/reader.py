import random
from typing import Dict, Union

from am3000_reader.attributes import AM3000Definitions, AM3000Attributes
from modbus_reader.reader import Reader
from modbus_reader.utils import timeit


class AM3000Reader(Reader):

    definition_class = AM3000Definitions
    attributes_enum = AM3000Attributes
    attributes = [
        AM3000Attributes.l1_voltage,
        AM3000Attributes.l2_voltage,
        AM3000Attributes.l3_voltage,
        AM3000Attributes.l1_current,
        AM3000Attributes.l2_current,
        AM3000Attributes.l3_current,
        AM3000Attributes.l1_active_power,
        AM3000Attributes.l2_active_power,
        AM3000Attributes.l3_active_power,
        AM3000Attributes.total_active_power,
        AM3000Attributes.total_active_energy,
    ]

    convert_to_kWh = [
        AM3000Attributes.l1_active_power,
        AM3000Attributes.l2_active_power,
        AM3000Attributes.l3_active_power,
        AM3000Attributes.total_active_power,
        AM3000Attributes.total_active_energy,
    ]

    @timeit
    def read(self) -> Dict[AM3000Attributes, Union[int, float]]:
        response = super().read()

        for attribute in self.convert_to_kWh:
            response[attribute] /= 1000

        return response

    def generate_mock_value(self, attribute):
        return {
            AM3000Attributes.l1_voltage: 230 * random.uniform(0.999, 1.001),
            AM3000Attributes.l2_voltage: 230 * random.uniform(0.999, 1.001),
            AM3000Attributes.l3_voltage: 230 * random.uniform(0.999, 1.001),
            AM3000Attributes.l1_current: 15 * random.uniform(0.9, 1.1),
            AM3000Attributes.l2_current: 15 * random.uniform(0.9, 1.1),
            AM3000Attributes.l3_current: 15 * random.uniform(0.9, 1.1),
            AM3000Attributes.l1_active_power: 10 * random.uniform(0.8, 1.2),
            AM3000Attributes.l2_active_power: 10 * random.uniform(0.8, 1.2),
            AM3000Attributes.l3_active_power: 10 * random.uniform(0.8, 1.2),
            AM3000Attributes.total_active_power: 30 * random.uniform(0.8, 1.2),
            AM3000Attributes.total_active_energy: 30 * random.uniform(0.8, 1.2),
        }[attribute]
