from collections import defaultdict
from enum import IntEnum

from modbus_reader.types import DataTypes, Definition


class Pro1Attributes(IntEnum):
    """Very incomplete list of attributes"""

    voltage = 0x2000
    current = 0x2060
    active_power = 0x2080
    active_energy = 0x3000


class Pro1Definitions:
    def __init__(self):
        # all of the used attributes are floats
        self.definition = defaultdict(lambda: (DataTypes.FLOAT, 2))

    def __call__(self, attribute: Pro1Attributes) -> Definition:
        return Definition(*self.definition[attribute])
