import random
from ipaddress import IPv4Address
from pprint import pprint

from modbus_reader.reader import Reader
from pro1_reader.attributes import Pro1Definitions, Pro1Attributes


class Pro1Reader(Reader):

    attributes = [
        Pro1Attributes.voltage,
        Pro1Attributes.current,
        Pro1Attributes.active_power,
        Pro1Attributes.active_energy,
    ]
    attributes_enum = Pro1Attributes
    definition_class = Pro1Definitions

    def generate_mock_value(self, attribute):
        return {
            Pro1Attributes.voltage: 230 * random.uniform(0.999, 1.001),
            Pro1Attributes.current: 15 * random.uniform(0.9, 1.1),
            Pro1Attributes.active_power: 10 * random.uniform(0.8, 1.2),
            Pro1Attributes.active_energy: 30 * random.uniform(0.8, 1.2),
        }[attribute]


if __name__ == "__main__":

    reader = Pro1Reader(IPv4Address("10.0.0.151"))

    pprint(reader.read())
