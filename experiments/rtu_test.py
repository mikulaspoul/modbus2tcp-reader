from pymodbus.client.sync import ModbusTcpClient
from pymodbus.framer.rtu_framer import ModbusRtuFramer

import logging
import sys

root = logging.getLogger()
root.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
ch.setFormatter(formatter)
root.addHandler(ch)

if __name__ == "__main__":

    client = ModbusTcpClient(host="10.0.0.151", port=10001, framer=ModbusRtuFramer)

    # 00 03 10 00 00 04 41 18
    result = client.read_holding_registers(0x1000, 4, unit=1)
    # 01 03 08 18 04 09 53 01 03 00 01 64 14

    print(result.registers)
