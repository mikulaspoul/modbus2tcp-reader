from pymodbus.client.sync import ModbusTcpClient

import logging
import sys

root = logging.getLogger()
root.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
ch.setFormatter(formatter)
root.addHandler(ch)

if __name__ == "__main__":

    client = ModbusTcpClient(host="10.0.0.151", port=502)

    # 00 01 00 00 00 06 00 03 10 00 00 04
    result = client.read_holding_registers(0x1000, 4, unit=1)

    print(result.registers)
