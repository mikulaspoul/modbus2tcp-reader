import pytest
from django.urls import reverse
from django.utils import timezone

from meter.models import Meter, Record


@pytest.mark.django_db
def test_export(client):
    meter = Meter.objects.create(reader_type="pro1", ip="127.0.0.1")

    Record.objects.create(
        meter=meter,
        created=timezone.now(),
        l1_voltage=0.1,
        l2_voltage=0.1,
        l3_voltage=0.1,
        l1_current=0.1,
        l2_current=0.1,
        l3_current=0.1,
        l1_active_power=0.1,
        l2_active_power=0.1,
        l3_active_power=0.1,
        total_active_power=0.1,
        total_active_energy=0.1,
        serial_number="13",
    )

    response = client.get(reverse("meter:export", args=[meter.ip]))
    assert response.status_code == 200

    # multiple phases
    meter.reader_type = "pro380"
    meter.save()

    response = client.get(reverse("meter:export", args=[meter.ip]))
    assert response.status_code == 200
