from unittest import mock
from unittest.mock import MagicMock

import pytest
from django.urls import reverse
from pymodbus.client.sync import ModbusTcpClient

from meter.models import Meter, VaconNXRecord


def side_effect(register, *args, **kwargs):
    values = {10310: 4001, 10309: 131, 10313: 560, 10304: 5001, 10303: 1500, 10301: 1201, 10302: 1401}[register]

    x = MagicMock()
    x.registers = [values]
    x.isError = lambda *_: False

    return x


@pytest.mark.django_db
def test_read():
    meter = Meter.objects.create(reader_type="vacon_nx", ip="127.0.0.1")

    with mock.patch.object(ModbusTcpClient, "read_holding_registers", side_effect=side_effect):
        meter.read()

    record = VaconNXRecord.objects.get()
    assert record.voltage_dc == "560"
    assert record.voltage_ac == "400.1"
    assert record.current == "13.1"
    assert record.frequency == "50.01"
    assert record.speed == "1500"
    assert record.torque == "120.1"
    assert record.power == "140.1"


@pytest.mark.django_db
def test_export(client):
    meter = Meter.objects.create(reader_type="vacon_nx", ip="127.0.0.1")

    with mock.patch.object(ModbusTcpClient, "read_holding_registers", side_effect=side_effect):
        meter.read()

    response = client.get(reverse("meter:export", args=[meter.ip]))
    assert response.status_code == 200
